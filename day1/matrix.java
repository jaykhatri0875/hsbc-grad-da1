public class matrix {

    static int initialValue = 10;

    public static void main(String[] args) {
        int row = 5,col = 5;
        int[][] matrix = new int[row][col];
        assignMatrix(matrix);
        printMatrix(matrix);
    }

    public static void assignMatrix(int[][] matrix) {
        for (int rowIn = 0; rowIn < 5; rowIn++) {
            for (int colIn = 0; colIn < 5; colIn++) {
                matrix[rowIn][colIn] = initialValue++;
            }
        }
    }

    public static void printMatrix(int[][] matrix) {
        for (int rowIn = 0; rowIn < 4; rowIn++) {
            for (int colIn = 0; colIn < 4; colIn++) {
                System.out.print("\t" + matrix[rowIn][colIn] + "\t");
            }
            System.out.println();
        }
    }
}