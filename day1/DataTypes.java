

public class DataTypes{

    //primitive data tyeps
    /*
        variable name rules:
        1. can be alphanumeric
        2. only _ and $ allowed
        3. cant start with digit
        4. cant use reserve 
        5. case sensitive
        
        conventions:
        1. use descriptive variable name
                 (no single letter variable names)
        2. use camelcase
        3. avoid _ and $
        
    */
    public static void main(String args[]){
        /*
        byte
        short 
        int
        long

        float
        double

        boolean

        char

        */
        int value;
        value=50;
        int daysInYear = 365;
        byte noofdaysinMonth = 30;
        boolean isa = false;
        char c = 'A';
        String name = "NOICE";

        System.out.println(name);




    }
}