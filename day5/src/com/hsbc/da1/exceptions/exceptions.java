package com.hsbc.da1.exceptions;

public class exceptions {
	public static void main(String[] args) {
		int a = 10;
		int b = 0;
		int array[] = {1,2,3,4};
		try {
			
			//try block will make sure that everything is going good 
			// in case of exception it will be catched by catch block to prevent abrupt code breaking !
			System.out.println("division is : "+b/a);
			System.out.println("array of index"+array[-1]);
			
		}
		
		// use multiple catch blocks to tackle diff scenarios diff ways
		catch(ArithmeticException e) {
			System.out.println("divide by zero error man");
			
		}
		
		//in the end use exception block in case if any other kind of error occures
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
			
		}
		
		
		
		
		
		finally {
			
			// this block is used to close connections in any case
			// highest priority things to do
			System.out.println("lol here we are");
		}
		
	}
}
