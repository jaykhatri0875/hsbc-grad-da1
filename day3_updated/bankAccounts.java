/*  
    salaried account :
    withdrawl limit is 15k
    min balance 0
    eligible for loan upto 10 L

    savings account:
    withdrawl limit = 15k
    min balance = 10k
    eligible for loan upto 5L

    current accoount:
    withdrawl = NA
    min balance = 25k 
    loan upto 25L

*/

abstract class BankAccount{

    public String customerName;
    public int mobileNumber;
    public int accountID;
    public double accountBalance;
    public static int counter= 100000;

    public BankAccount(String name,int num,double accountBalance){
        this.customerName = name;
        this.mobileNumber = num;
        this.accountBalance = accountBalance;
        this.accountID = ++counter;
    }
    
    public abstract void getLoan(double loanAmount);

   

    public abstract double withdraw(double amount);


}

class SalaryAccount extends BankAccount{

    private int minBalance = 0;
    
    private String panNum;
    public SalaryAccount(String name,int num,double accountBalance,String panNum){
        super(name,num,accountBalance);
        this.panNum = panNum;
    }

    public void getLoan(double loanAmount){
        if(loanAmount<=1000000){
        System.out.printf("loan granted of amount &d",loanAmount);
        }
    }

   

    public double withdraw(double amount){
         if((this.accountBalance-amount)>=minBalance){
             this.accountBalance -= amount;
             return this.accountBalance; 
         }
         return 0;
    }
}


class SavingsAccount extends BankAccount{

    private int minBalance = 10000;

    public SavingsAccount(String name,int num,double amount){
        super(name,num,amount);
    }

    public void getLoan(double loanAmount){

        if(loanAmount<=2500000){
        System.out.printf("loan granted of amount &d",loanAmount);
        }
    }

    

    public double withdraw(double amount){
         if((this.accountBalance-amount)>=minBalance){
             this.accountBalance -= amount;

             return this.accountBalance; 
         }
         return 0;

    }
}


class CurrentAccount extends BankAccount{

    private int minBalance = 2500000;
    private String panNum;
    private String GSTnum;
    public CurrentAccount(String name,int num,double accountBalance,String panNum,String GSTnum){
        super(name,num,accountBalance);
        this.panNum = panNum;
        this.GSTnum = GSTnum;

    }
    public void getLoan(double loanAmount){

        if(loanAmount<=2500000){
        System.out.printf("loan granted of amount &d",loanAmount);
        }
    }

    
    public double withdraw(double amount){

         if((this.accountBalance-amount)>=minBalance){
             
            this.accountBalance = this.accountBalance - amount;

             return this.accountBalance; 
         }
         return 0;


    }



}

public class bankAccounts{
    public static void main(String[] args){

        BankAccount jay = null;

        String type = args[0];
//String name,int num,double accountBalance,String panNum
        if(type.equals("1")){
            jay = new SavingsAccount("jay",1234,1234500);
        }
        else if(type.equals("2")){
            jay = new SalaryAccount("jay",1234,1234500,"123abc");
        }
        else if(type.equals("3")){
            jay = new CurrentAccount("jay",1234,1234500,"xyz1010","abcd1234");
        }

        System.out.println("done lora");

    }

    
    
}
