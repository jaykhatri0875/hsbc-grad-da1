interface InsuranceGateway{
    
    double calculatePremium(String vehicleName,double insuredAmount,String model);

    double payPremium(double preminumAmount,String vehicleNumber,String model);
}

class BajajInsurance implements InsuranceGateway{
    private static int counter = 10000;
    private double preminumAmount;
    private int policyNumber;
    public double calculatePremium(String vehicleName,
                                   double insuredAmount,
                                   String model){
            preminumAmount = insuredAmount*0.3;
            System.out.println("---calculating premium by BAJAJ_INSURANCE---");
            return preminumAmount;
    }

    public double payPremium(double preminumAmount,
                      String vehicleNumber,
                      String model){

            System.out.println("payment of "+preminumAmount+"vehicle number: "+vehicleNumber+" model "+model+" is done.");
            policyNumber = ++ counter;
            System.out.println("your policy number is : "+policyNumber);
            return policyNumber;
    }

}

class TataAIG implements InsuranceGateway{

    private static int counter = 20000;
    private double preminumAmount;
    private int policyNumber;
    public double calculatePremium(String vehicleName,
                                   double insuredAmount,
                                   String model){
            preminumAmount = insuredAmount*0.25;
            System.out.println("---calculating premium by TATA_AIG---");
            System.out.println("premium ammount : "+preminumAmount);
            return preminumAmount;
    }
    public double payPremium(double preminumAmount,
                      String vehicleNumber,
                      String model){

            System.out.println("payment of "+preminumAmount+"vehicle number: "+vehicleNumber+" model "+model+" is done.");
            policyNumber = ++ counter;
            System.out.println("your policy number is : "+policyNumber);
            return policyNumber;
    }

}



public class insurance{
    public static void main(String[] args) {

        double insuranceAmount;

        InsuranceGateway insureGate = null;
        String arg = args[0];
        if(arg.equals("1")){
            BajajInsurance newInsuranceBajaj = new BajajInsurance();
            insureGate = newInsuranceBajaj;
        }
        else{
            TataAIG newInsuranceTata = new TataAIG();
            insureGate = newInsuranceTata;
        }
        insuranceAmount = insureGate.calculatePremium("ALTO",10000,"XY1234");
        System.out.println(insuranceAmount);
    }
}