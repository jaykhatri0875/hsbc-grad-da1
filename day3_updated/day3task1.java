/*  
    salaried account :
    withdrawl limit is 15k
    min balance 0
    eligible for loan upto 10 L

    savings account:
    withdrawl limit = 15k
    min balance = 10k
    eligible for loan upto 5L

    current accoount:
    withdrawl = NA
    min balance = 25k 
    loan upto 25L

*/

abstract class BankAccount{

    public String customerName;
    public int mobileNumber;
    public int minBalance;
    public int accountID;
    public double accoountBalance;
    public static int counter= 100000;

    public BankAccount(String name,int num,double accoountBalance){
        this.customerName = name;
        this.mobileNumber = num;
        this.accoountBalance = accoountBalance;
        //this.accoundID = ++counter;
    }
    
    public abstract void getLoan(double loanAmount);

    public abstract boolean validate(double amonut);

    public abstract double withdraw(double amonut);


}

class SalaryAccount extends BankAccount{

    private int minBalance = 0;
    
    private String panNum;
    public SalaryAccount(String name,int num,double accountBalance,String panNum){
        super(name,num,accountBalance);
        this.panNum = panNum;
    }

    public void getLoan(double loanAmount){
        if(loanAmount<=1000000){
        System.out.printf("loan granted of amount &d",loanAmount);
        }
    }

    public boolean validate(double amonut){
        return (this.accountBalance-amonut)>= minBalance;
    }

    public double withdraw(double amonut){
         if(validate(amount)){
             this.accoountBalance -= amonut;
             return this.accoountBalance; 
         }


    }
}

class SavingsAccount extends BankAccount{

    private int minBalance = 10000;

    public void getLoan(double loanAmount){

        if(loanAmount<=2500000){
        System.out.printf("loan granted of amount &d",loanAmount);
        }
    }

    public boolean validate(double amonut){
        return (this.accountBalance-amonut)>= minBalance;
    }

    public double withdraw(double amonut){
         if(validate(amount)){
             this.accoountBalance -= amonut;

             return this.accoountBalance; 
         }


    }
}


class CurrentAccount extends BankAccount{

    private int minBalance = 25000;
    private String panNum;
    private String GSTnum;
    public CurrentAccount(String name,int num,double accountBalance,String panNum,String GSTnum){
        super(name,num,accountBalance);
        this.panNum = panNum;
        this.GSTnum = GSTnum;

    }
    public void getLoan(double loanAmount){

        if(loanAmount<=2500000){
        System.out.printf("loan granted of amount &d",loanAmount);
        }
    }

    public boolean validate(double amonut){
        return (this.accountBalance-amonut)>= minBalance;
    }

    public double withdraw(double amonut){
         if(validate(amount)){
             this.accoountBalance -= amonut;

             return this.accoountBalance; 
         }


    }



}

public class day3task1{
    public static void main(String[] args){

        BankAccount jay = null;

        String type = args[0];

        if(type.equals("1")){
            jay = new SavingsAccount();
        }
        else if(type.equals("2")){
            jay = new SalaryAccount();
        }
        else if(type.equals("3")){
            jay = new CurrentAccount();
        }
    }

    
    
}
