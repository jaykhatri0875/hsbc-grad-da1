// interface among muktiple payment gateways

interface paymentGateway{
    void makepayment(String sender,String receiver,double amount,String notes);
}

interface MobileRecharge{
    void recharge(int number,double amount);
}

class gpay implements paymentGateway,MobileRecharge{
    public void makepayment(String sender, String receiver, double amount ,String notes){
        System.out.println("Payment from :"+sender+" to "+receiver+"is done by gpay \n and notes "+notes);
    }

    public recharge(int number,double amount){
        System.out.println("Recharge on "+number+" amount "+amount+" is done by gpay");
    }
}

class jiopay implements paymentGateway,MobileRecharge{
     public void makepayment(String sender, String receiver, double amount ,String notes){
        System.out.println("Payment from :"+sender+" to "+receiver+"is done by jiopay \n and notes "+notes);
    }

    public recharge(int number,double amount){
        System.out.println("Recharge on "+number+" amount "+amount+" is done by jiopay");
    } 
}


public class interfaceClient{
    public static void main(String[] args){
        paymentGateway paymentgateway =null;
        MobileRecharge mobilerecharge = null;
        if(args[0]=="1"){
            gpay gpay = new gpay();
            paymentgateway = gpay;
            mobilerecharge = gpay;

        }

    else if(args[0]=="2"){
        jiopay jpay = new jiopay();
        paymentgateway = jpay;
        mobilerecharge = jpay;
    }

    paymentgateway.makepayment("jay","khatri",1000,"udhar");
    mobilerecharge.recharge("8981829738917",100);
    }
}
