public class callbyValRef{
    public static void main(String[] args) {
        int array[] = new int[]{10,20,30,40};
        int x=100;
        int y=200;
        System.out.printf("before call &d &d",x,y);
        callByValue(x,y);
        
        
        System.out.println("------");
        for (int i : array) {
            System.out.println(i);
        }

        callByReference(array);
    }

    public static void callByValue(int x,int y){
        x=10;
        y=10;
        System.out.printf("inside callbyvalue &d &d",x,y);
    }
    public static void callByReference(int[] arr){
        arr[0] = 0;
        arr[1] = 0;
        arr[2] = 0;
        arr[3] = 0;
        
        System.out.printf("inside callbyreference function");
        for(int a:arr){      
          System.out.println(a);
          }
    }
}