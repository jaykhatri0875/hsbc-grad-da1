public class address{
    private String street;
    private String city;
    private int zipCode;
    private String state;

    public address(String street, String city,String state,int zipCode){
        this.street=street;
        this.state = state;
        this.city=city;
        this.zipCode=zipCode;
        
    }

    public String getStreet(){
        return street;
    }

    public void setStreet(String street){
        this.street = street;
    }
    public String getCity(){
        return city;
    }
    public void setCity(String city){
        this.city = city;
    }
    public int getzipCode(){
        return zipCode;
    }

    public void setzipCode(int zipCode){
        this.zipCode=zipCode;
    }

    public String getState(){
        return state;
    }
    public void setState(String state){
        this.state=state;
    }
}