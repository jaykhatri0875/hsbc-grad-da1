public class CurrentAccount{
    public static long accountNumberTracker = 5000;
    private long accountnumber;

    private double accountBalance;
    private String gstNumber;
    private String customerName;
    private String businessName;
    private String communicationAddress;
    private String city;
    private  int zipcode;
    private long minBalance=50000;
    address Address;
    public long getAccountNumber(){
        return accountnumber;
    }

    public CurrentAccount(String customerName,
                          double initialAccountbalance,
                          String gstNumber,
                          String bname,
                          int zipCode,
                          String communicationAddress){
        this.customerName = customerName;
        this.gstNumber = gstNumber;
        this.businessName = bname;
        if(initialAccountbalance>=minBalance){
        this.accountBalance = initialAccountbalance;
        }
        this.communicationAddress=communicationAddress;
        this.zipcode = zipcode;
        this.accountnumber=++accountNumberTracker;
    }

    public CurrentAccount(String customerName,
                          double initialAccountbalance,
                          String gstNumber,
                          String bname,
                          address commAddress){
        this.customerName = customerName;
        this.gstNumber = gstNumber;
        this.businessName = bname;
        if(initialAccountbalance>=minBalance){
        this.accountBalance = initialAccountbalance;
        }
        this.Address=commAddress;
        this.zipcode = zipcode;
        this.accountnumber=++accountNumberTracker;
    }



    public double withdraw(double amount){
        this.accountBalance -= amount;
        return amount; 
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public double deposit(double amount){
        this.accountBalance += amount;
        return accountBalance;

    }

    public void updateAddress(String newaddress, int zipCode, String city) {

        this.communicationAddress = newaddress;
        this.zipcode = zipcode;
        this.city = city;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public double transferAmount(double amount,CurrentAccount user){
        
        if(amount<this.accountBalance){
            this.accountBalance -= amount;
            user.deposit(amount);
            return this.accountBalance;
        }
        return 0;

    }

}


