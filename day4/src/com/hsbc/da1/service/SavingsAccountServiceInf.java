package com.hsbc.da1.service;

import java.util.List;

import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.exception.invalidAccountIDException;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountServiceInf {
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance);
	public void deleteSavingsAccount(long accountNumber);
	public List<SavingsAccount> fetchSavingsAccounts();
	public SavingsAccount fetchSavingsAccountByAccountID(long accountNumber) throws invalidAccountIDException;
	public double withdraw(long accountId,double amount) throws InsufficientBalanceException , invalidAccountIDException;
	public double deposit(long accountId,double amount) throws invalidAccountIDException;
	public double checkBalance(long accountId) throws invalidAccountIDException;
	public void transfer(long senderID,long receiverID,double amount) throws InsufficientBalanceException,invalidAccountIDException;
	
}
