package com.hsbc.da1.service;

import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.exception.invalidAccountIDException;
// service layer will be used to apply business logic & validations 

import com.hsbc.da1.model.SavingsAccount;

import java.util.List;

import com.hsbc.da1.dao.ListSavingsAccountDAO;



public class SavingsAccountService implements SavingsAccountServiceInf {
	
	private ListSavingsAccountDAO daoAccount = new ListSavingsAccountDAO();
	
	
	public SavingsAccount createSavingsAccount(String customerName,double accountBalance) {
		//no validations for now
		SavingsAccount savingsAccount = new SavingsAccount(customerName,accountBalance);
		SavingsAccount savingsAccountCreated = this.daoAccount.createSavingsAccount(savingsAccount);
		
		return savingsAccountCreated;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		this.daoAccount.deleteSavingsAccount(accountNumber);
		
		
	}
	
	public List<SavingsAccount> fetchSavingsAccounts() {
		
		return  this.daoAccount.fetchSavingsAccounts();
		
	}
	
	public SavingsAccount fetchSavingsAccountByAccountID(long accountNumber) throws invalidAccountIDException {
		SavingsAccount SA = this.daoAccount.fetchSavingsAccountByAccountID(accountNumber);
		return SA;
		
			
	}
	
	public double withdraw(long accountId,double amount) throws InsufficientBalanceException , invalidAccountIDException {
		SavingsAccount sa = this.daoAccount.fetchSavingsAccountByAccountID(accountId);
		if(sa!=null) {
			double currentAccBalance = sa.getAccountBalance();
			if(currentAccBalance >= amount) {
				currentAccBalance = currentAccBalance - amount;
				sa.setAccountBalance(currentAccBalance);
				this.daoAccount.updateSavingsAccount(accountId, sa);
				return sa.getAccountBalance();
			}
			else {
				throw new InsufficientBalanceException("not sufficient balance");
			}
			
		}
		return 0;
	}
	
	public double deposit(long accountId,double amount) throws invalidAccountIDException {
		SavingsAccount sa = this.daoAccount.fetchSavingsAccountByAccountID(accountId);
		if(sa!=null) {
			double currentAccBalance = sa.getAccountBalance();
			
				currentAccBalance = currentAccBalance + amount;
				sa.setAccountBalance(currentAccBalance);
				this.daoAccount.updateSavingsAccount(accountId, sa);
				return sa.getAccountBalance();
		}
			return 0;
			
	}
	
	
	public double checkBalance(long accountId) throws invalidAccountIDException {
		SavingsAccount sa = this.daoAccount.fetchSavingsAccountByAccountID(accountId);
		if(sa!=null) {
			return sa.getAccountBalance();
		}
		return 0;
	}
	
	public void transfer(long senderID,long receiverID,double amount) throws InsufficientBalanceException,invalidAccountIDException {
		SavingsAccount senderAcc = this.daoAccount.fetchSavingsAccountByAccountID(senderID);
		SavingsAccount recvAcc = this.daoAccount.fetchSavingsAccountByAccountID(receiverID);
		double updatebalance = this.withdraw(senderAcc.getAccountNumber(), amount);
		if(updatebalance!=0) {
			this.deposit(recvAcc.getAccountNumber(),amount);
		}
	}
	
}
