package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hsbc.da1.exception.invalidAccountIDException;
import com.hsbc.da1.model.SavingsAccount;

public class ListSavingsAccountDAO implements SavingsAccountDAOInf {
		private static List<SavingsAccount> savingsAccountList = new ArrayList<>();
		private static int counter;
		
		@Override
		public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
			//savingAccounts[counter++] = savingsAccount;
			this.savingsAccountList.add(savingsAccount);
			return savingsAccount;
		}
		
		@Override
		public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount savingsAccount) {
			for(SavingsAccount sa : savingsAccountList) {
				if(sa.getAccountNumber()==accountNumber) {
					sa=savingsAccount;
				}
			}
			return savingsAccount;
		}
		
		@Override
		public void deleteSavingsAccount(long accountNumber) {
			for(SavingsAccount sa : savingsAccountList){
				if(sa.getAccountNumber()==accountNumber) {
					this.savingsAccountList.remove(sa);
				}
				
				
			}
		}
		@Override
		public List<SavingsAccount> fetchSavingsAccounts() {
			return this.savingsAccountList;
		}
		@Override
		public SavingsAccount fetchSavingsAccountByAccountID(long accountNumber) throws invalidAccountIDException {
			for(SavingsAccount sa: savingsAccountList) {
				if(sa.getAccountNumber()==accountNumber) {
					return sa;
				}
			}
			return null;
		}
	

}
