package com.hsbc.da1.dao;
//this is data accesss layer 
// used to store and access data from dto layer
import com.hsbc.da1.model.SavingsAccount;


import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.exception.invalidAccountIDException;

public class SavingsAccountDAO {
	//should implement SavingsAccountDAOINfs
	private static SavingsAccount[] savingAccounts = new SavingsAccount[100];
	private static int counter;
	
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount) {
		savingAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}

	public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount updatedSavingsAccount) {
		for(int index=0;index<savingAccounts.length;index++) {
			if(savingAccounts[index].getAccountNumber() == accountNumber) {
				savingAccounts[index] = updatedSavingsAccount;
				break;
			}
		}
		return updatedSavingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		for(int index=0;index<savingAccounts.length;index++) {
			if(savingAccounts[index].getAccountNumber() == accountNumber) {
				savingAccounts[index] = null;
				break;
			}
			
		}
	}
	/*
	public SavingsAccount[] fetchSavingsAccounts() {
		return savingAccounts;
	}*/
	
	public SavingsAccount fetchSavingsAccountByAccountID(long accountNumber) throws invalidAccountIDException {
		for(int index=0;index < savingAccounts.length;index++) {
			if(savingAccounts[index].getAccountNumber()==accountNumber) {
				return savingAccounts[index];
			}
		}
		throw new invalidAccountIDException("customer not found");
	}
}
