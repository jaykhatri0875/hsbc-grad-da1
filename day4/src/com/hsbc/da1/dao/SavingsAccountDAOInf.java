package com.hsbc.da1.dao;
import java.util.List;
import com.hsbc.da1.exception.invalidAccountIDException;
import com.hsbc.da1.model.SavingsAccount;

public interface SavingsAccountDAOInf {
	public SavingsAccount createSavingsAccount(SavingsAccount savingsAccount);
	public SavingsAccount updateSavingsAccount(long accountNumber,SavingsAccount updatedSavingsAccount);
	public void deleteSavingsAccount(long accountNumber);
	public List<SavingsAccount> fetchSavingsAccounts();
	public SavingsAccount fetchSavingsAccountByAccountID(long accountNumber) throws invalidAccountIDException;
}
