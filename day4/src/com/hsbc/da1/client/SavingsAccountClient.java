package com.hsbc.da1.client;

import java.util.List;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.model.SavingsAccount;

import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.exception.invalidAccountIDException;

public class SavingsAccountClient {
	
		public static void main(String[] args) throws InsufficientBalanceException ,invalidAccountIDException {
			SavingsAccountController controller = new SavingsAccountController();
			SavingsAccount jaySA = controller.openSavingsAccount("jay k", 100000);
			SavingsAccount KhatriSA = controller.openSavingsAccount("khatri j", 50000);
			SavingsAccount sa1 = controller.openSavingsAccount("sample 1", 1234); 
			SavingsAccount sa2 = controller.openSavingsAccount("sample 2", 7070);
			SavingsAccount sa3 = controller.openSavingsAccount("sample 3", 2020);
			SavingsAccount sa4 = controller.openSavingsAccount("sample 4", 4000);
			//System.out.println("accountid jay : "+jaySA.getAccountNumber());
			//System.out.println("accountid k jay :"+KhatriSA.getAccountNumber()+" balance: "+KhatriSA.getAccountBalance());
			
			List<SavingsAccount> newsa = controller.fetchSavingsAccounts();
			int count=1;
			for(SavingsAccount sa : newsa) {
				if(sa!=null) {
					System.out.println(count+". id:"+sa.getAccountNumber()+" balance: "+sa.getAccountBalance()+"\n");
					count++;
				}
			}
			try {
			controller.transfer(20020,20002,5000);
			}
			catch(InsufficientBalanceException | invalidAccountIDException e) {
				System.out.println(e);
			}
			System.out.println("-------------");
			for(SavingsAccount sa : newsa) {
				if(sa!=null) {
					System.out.println(count+". id:"+sa.getAccountNumber()+" balance: "+sa.getAccountBalance()+"\n");
					count++;
				}
			}
			
			
			
		}
	
}
