package com.hsbc.da1.controller;

import com.hsbc.da1.service.SavingsAccountService;

import com.hsbc.da1.model.SavingsAccount;

import java.util.List;

import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.exception.invalidAccountIDException;

public class SavingsAccountController {
	
	private SavingsAccountService savingsAccService = new SavingsAccountService();
	
	public SavingsAccount openSavingsAccount(String customerName,double accountBalance) {
	
			SavingsAccount savingsAccount = this.savingsAccService.createSavingsAccount(customerName, accountBalance);
			return savingsAccount;
	
	}
	public void deleteSavingsAccount(long accountNumber) {
			this.savingsAccService.deleteSavingsAccount(accountNumber);
	
	}
	
	public List<SavingsAccount> fetchSavingsAccounts() {
		return  this.savingsAccService.fetchSavingsAccounts();
		
	}
	
	public double withdraw(long accountId,double amount) throws InsufficientBalanceException,invalidAccountIDException {
		return this.savingsAccService.withdraw(accountId, amount);
	}
	
	public double deposit(long accountId,double amount) throws invalidAccountIDException {
		return this.savingsAccService.deposit(accountId, amount);
	}
	
	public double checkBalance(long accountId) throws invalidAccountIDException{
		return this.savingsAccService.checkBalance(accountId);
	}
	public void transfer(long senderID,long receiverID,double amount) throws InsufficientBalanceException,invalidAccountIDException{
		this.savingsAccService.transfer(senderID, receiverID, amount);
	}
	
	
	
}