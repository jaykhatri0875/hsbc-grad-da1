package com.hsbc.da1.model;
//this is model - DTO layer
// this layer will be used to communicate with database
public class SavingsAccount {
	
	private String customerName;
	private long accountNumber;
	private double accountBalance;
	private static long counter =20000;
	
	public SavingsAccount(String customerName,double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++ counter;
		
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}
}
